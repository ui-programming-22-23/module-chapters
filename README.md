# UI Programming module 2022 - 2023

* 01-web-basics-and-git
* 02-web-forms_data-reception
* 03-canvas
* 04-scope-vars-lets-consts
* 05-audio_and_canvas-transformations
* 06-animations
* 07-event-listeners
* 08-buttons-and-dpads
* 09-spritesheets
* _010-webStorage
* _011-gitlab-pages
* _012-PWA
* PROJECT

## CA (continuous assessment / practicals) submission procedure

To submit your practicals, follow this process:

1. Complete the code writing necessary for the practical and upload it to the `ui-programming-22-23` Gitlab.com group.

2. Assure that your project is correctly named. You should title your practicals following this pattern:

```
ChapterNumber_ChapterTitle_FirstName_LastName
```

Example:
07_event-listeners_Colm_ONeill

3. Paste a link to your project in the corresponding column in the submission spreadsheet: 

https://instituteoftechnol663-my.sharepoint.com/:x:/g/personal/oneillco_itcarlow_ie/EVZm9Fa3-mdEjaz_Q0R-PIkB_ykthm0UF8Ji3FB2OCzY0g?e=QYZwTW

## grading scheme

#### 0 – 35% Basic

You will obtain a grade in this range if you:

- deliver only parts of the practical
- push a repo to gitlab
- post a link to the CA spreadsheet

#### 35 - 75% Intermediate

You will obtain a grade in this range if you:

- meet all the specs in the briefing
- display a genuine understanding of the themes and practices of the practical
- surpass the practical requirements and display an ability to research and add to the requirements


#### 75 - 100 Advanced

You will obtain a grade in this range if you:

- surpass the practical by demonstrating particular interest in the subjects by adding a lot to the brief, relevant content, advanced styling and genuine interest.

