
function pageLoaded(){
    
    let canvas = document.getElementById("canvas");
    let context = canvas.getContext("2d"); //2d context for canvas

    context.translate(550, 250);

    for (let loop = 0; loop <= 24; loop++)
    {
        context.strokeStyle = "#00FF00";
        context.strokeRect(75, (loop * 3), 125, (loop * 1.5));
        context.rotate(loop * Math.PI / 180);
    }

    // method 1, get sound from HTML document
    // let newSound = document.getElementById("audio");
    // method 2, set sound in JS directly
    let newSound = new Audio("assets/audio/mixkit-squeak-notification-1017.wav");
    // note that the new Sound() object differs from the new Image() object
    // in that we set the URL directly when we set the object, we don't 
    // call .src in a second line
    
    console.log(newSound);

    let step = 0;
    
    setInterval(playSound, 2000);
 
    function playSound(){
        console.log("calling sound loop");
        if (step < 5)
        {
            newSound.play();
            step++;
        };
    }

}

pageLoaded();
console.log("end of script");