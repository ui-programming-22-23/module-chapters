# Canvas transformations + audio content

## Task 1:

Display your understanding of canvas transformations by creating a generative image, using examples from the following resource:

https://webplatform.github.io/docs/tutorials/canvas/Canvas_tutorial/Transformations/

Make sure your code is up to date with ES6 so use `let` and `const` and avoid `var` when possible.

## Task 2:

Find a method where you can repeat a sound automatically a specific number of times. If your generative drawing from task 1 uses a loop that repeats 10 times, try to get a sound to play an equal amount of times.