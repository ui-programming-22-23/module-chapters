console.log("hello from script");

function pageLoaded() {

    const canvas = document.getElementById("the_canvas");

    const context = canvas.getContext("2d");


    let newImg = new Image();
    newImg.src = "assets/img/tesse.gif"


    // remeber: you don't have to wait for an .onload event if you create the HTML Image object over in your .html doc,
    // we only have to create this function because we are creating the new Image() here in js.
    newImg.onload = function () {
        //simple draw
        context.drawImage(newImg, 0, 0, 50, 50);
        //transform skew
        context.drawImage(newImg, 250, 10, 200, 56);
        //show a part of the image
        //context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);
        // img   	Specifies the image, canvas, or video element to use 
        // sx    	(Optional.) The x coordinate where to start clipping 	
        // sy    	(Optional.) The y coordinate where to start clipping 	
        // swidth    	(Optional.) The width of the clipped image 	
        // sheight   	(Optional.) The height of the clipped image 	
        // x     	The x coordinate where to place the image on the canvas 	
        // x     	The x coordinate where to place the image on the canvas 	
        // y     	The y coordinate where to place the image on the canvas 	
        // width     	(Optional.) The width of the image to use (stretch or reduce the image) 	
        // height    	(Optional.) The height of the image to use (stretch or reduce the image)
        // check references here https://www.w3schools.com/tags/canvas_drawimage.asp
        context.drawImage(newImg, 10, 25, 60, 55, 520, 80, 120, 110);
        //translate the origin
        context.translate(50, 50);
        //now draw the same image as before, same coordinates, but it is translated because of the previous line
        context.drawImage(newImg, 10, 25, 60, 55, 520, 80, 120, 110);
        context.rotate(Math.PI / 6);
        context.drawImage(newImg, 350, 10, 200, 56);
        context.scale(1, 1);
        context.rotate(-Math.PI / 3);
    }
    



}

let btn = document.querySelector(".btn");
console.log(btn);

function playAudio(){
    let bounce = new Audio('assets/media/bounce.mp3');
    // In certain browsers (Edge, Chrome) you might get an `Uncaught (in promise) DOMException: play() failed` error.
    // setting a short timeout should solve the error
    bounce.play();
}

btn.onclick = playAudio;


pageLoaded();