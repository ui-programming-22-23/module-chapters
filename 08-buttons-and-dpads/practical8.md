# UI buttons, d-pads, healthbars and more

In this practical you will be required to research and implement various UI components for your game.

Add these four items to the code base you've developped in Practical 7

There will be 4 items to incorporate to your project:

### 1. a d-pad, for visual mouse movement interactions

To build your d-pad you could use the `HTML` and `CSS` below, or build a custom solution like the example in this code pen: https://codepen.io/thefewunshaken/pen/bjYWvB

Make the visual D-Pad be clickable and connect those actions to your character movement.

HTML
```
<table>
    <tbody>
      <tr>
        <td>&nbsp;</td>
        <td><button onclick="buttonOnClick()" class="button yellow round">Y</button></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><button onclick="buttonOnClick()" class="button blue round">X</button></td>
        <td>&nbsp;</td>
        <td><button onclick="buttonOnClick()" class="button red round">B</button></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><button onclick="buttonOnClick()" class="button green round">A</button></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
    </tbody>
  </table>
```

CSS
```
.button {
  background-color: #000000;
  border: none;
  box-shadow: 0 9px#505050;
  padding: 30px;
  text-align: center;
  font-size: 28px;
}

.button:hover {
  background-color: #3F3F3F;
}

.button:active {
  box-shadow: 0 5px #3D3D3D;
  transform: translateY(5px);
}

.red {
  color: #FF0000;
}

.green {
  color: #00FF00;
}

.blue {
  color: #0000FF;
}

.yellow {
  color: #FFFF00;
}

.round {
  border-radius: 50%;
}
```

### 2. a health bar

A health bar can also be used as a progress bar or a timer. A simple visual indecator of something growing or something being depleated.

Using simple `.fillRect()` in the `<canvas>` is it very easy to show a visual representation of health, progress, time, etc. 

Make the healthbar or timer have a function in your practical. If progress or health does not suit the gameplay you are building, build in a timer that counts down from a certain amount of seconds, urging the player to complete a task in a certain amount of time.

The following code should get you well on your way:

```
// Draw a HealthBar on Canvas, can be used to indicate players health
function drawHealthbar() {
  var width = 100;
  var height = 20;
  var max = 100;
  var val = 10;

  // Draw the background
  context.fillStyle = "#000000";
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillRect(0, 0, width, height);

  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(val / max, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);
}
```

### 3. a score counter

Similar to the above, giving visual feedback of progress through score is a very useful and playful UI element. Build a score counter into your developing game. Make the visual aesthetic of the score counter in keeping with the rest of the mood of the game. Your score-counter can count amounts of collected items, time spent on a task, amount of kills, etc.

What is the best way to make a score counter? Are you going to use the `.fillText()` function in the canvas or are you going to use a html element that is outside of the canvas?

### 4. an external library called nipple.js

https://yoannmoi.net/nipplejs/#demo

The link above brings you to a demo page that show the working of a mouse-based joystick.

Being able to integrate existing libraries into your code is a very powerful ability. Thanks to open source and copy-left licensing, some developers publish components and code-libraries like these for all of our benefit, developers, gamers, users.

Look at the demo and the documentation via the link above. Can you figure out how to implement this visual, mouse-based joystick in the practical? 
