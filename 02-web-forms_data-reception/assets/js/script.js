//console.log("hello from script");
function validateForm() {
    var x = document.forms["helloForm"]["name"].value;
    if (x == "") {
        alert("I need to know your name so I can say Hello");
        return false;
    }
    else{
        alert("Hello there " + document.forms["helloForm"]["name"].value);
        //more advanced pt2: make a system that changes the webpage based on the inputted name 
        addName();
    }
}

function addName() {
    var y = document.getElementById("main-header");
    var queryString = document.location.search;
    console.log(queryString);
    var name = queryString.split("=")[1];
    console.log(name);
    if (name == undefined){
        console.log("no name in URL query string")
    }
    else{
        y.innerHTML = "Hello " + name;
    }
}

addName();
