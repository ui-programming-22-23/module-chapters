# JS canvas

## Task 1

Build a web page that uses a ´canvas´ element. Use Javascript to draw the following items in the canvas itself:

* 4 filled blue squares in each corner of the canvas
* 5 other complex shapes, either as outlines or as filled shapes
* three text elements using 3 different fonts

### Reminder

![this is how the canvas system works](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes/canvas_default_grid.png)

[Here is a link to a cheat-sheet that recaps most of the things you'll need when using the canvas.](https://simon.html5.org/dump/html5-canvas-cheat-sheet.html)

## Task 2

Study the folder ´03-canvas/responsive-canvas´ and compare the differences between the two canvases specifically regarding the 'mobile responsiveness'. Before delivering your practical, port over the code that you need to make your canvas full of shapes and text responsive. 
