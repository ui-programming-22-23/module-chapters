# Practical 05A vars lets conts

This practical follows on from the information you recived that is recapped in the document <04-Scope-in-Java.md>.

## Task 1

Revisit your code from practicals 1, 2 and 3 and change your variables to use only `let` and `const`. If you use `var` it will be assumed that you need a global variable, therefor, you will need to justify this need during your practical demo.
