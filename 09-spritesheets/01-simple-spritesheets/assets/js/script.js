const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");


// draw function calls the animate function
function draw() {
  animate();
}

// Total Frames in our spritesheet, this will be specific to your sprite
let frames = 6;

// init current frame counter
let currentFrame = 0;

// Sprite
let sprite = new Image();
sprite.src = "assets/img/1to6.png"; // Frames 1 to 6

// Initial time set
let initial = new Date().getTime();
let current; // current time

function animate() {
  current = new Date().getTime(); // set current time to right now
  if (current - initial >= 5000) { // check is greater that 750 milliseconds
      currentFrame = (currentFrame + 1) % frames;
      console.log("currentFrame: " + currentFrame); 
      
      // currentFrame = (counter incrementing) : 6
      //  remainder operation (similar to a modulo, but not works differently with negative numbers)
      // % operator returns the remainder when the first number is divided by the second
      // console.log(7 % 3) => 1;
      // console.log(8 % 2) => 0;
      // console.log(9 % 2) => 1;
      // console.log(7.5 % 2) => 1.5;   
      initial = current; // reset initial
  } 

  // Draw sprite frame
  context.drawImage(sprite, (sprite.width / 6) * currentFrame, 0, 256, 256, 0, 0, 256, 256);

}

function gameloop() {
  //input();
  //update();
  draw();
  window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);