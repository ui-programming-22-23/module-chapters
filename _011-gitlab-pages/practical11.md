# Gitlab Pages and `.gitlab-ci.yml`

The following process details how to extend one of your gitlab repositories into a hosting provider. Similar to Github, Gitlab allows you to (with some configuration needed) host your HTML, CSS and JS in a public website, with a specific URL.

The steps to follow are quite simple:

### 1. Create a `.gitlab-ci.yml` file in the root of your repository

documentation link: https://docs.gitlab.com/ee/ci/quick_start/#create-a-gitlab-ciyml-file

All you need to do is to create a 'dot' file (file that starts with `.`) in the root of your repository, and give it the name: `.gitlab-ci.yml`

In this file, we will write (paste actually) some `yaml` code. Find out more about [YAML here](https://en.m.wikipedia.org/wiki/YAML)

The code to include in the .yml file is the following: 

```
pages:
  stage: deploy
  environment: production
  script:
    - mkdir .public
    - cp -r ./* .public
    - rm -rf public
    - mv .public public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

```

An example of this is located here: https://gitlab.com/ui-programming-22-23/10_web_storage_in_root_Colm_ONEILL with pages served here https://ui-programming-22-23.gitlab.io/10_web_storage_in_root_Colm_ONEILL/

### 1.1 OPTIONAL :: consider another repo organisation method

If you need the root of your repository to be kept clean you can configure Gitlab pages to 'serve' a subfolder of your project. An example of this is located at: https://gitlab.com/ui-programming-22-23/10_web_storage_in_sub_folder_Colm_ONEILL with pages served here https://ui-programming-22-23.gitlab.io/10_web_storage_in_sub_folder_Colm_ONEILL/

* For this config, you'll need to create a folder within your repo (best to call it `public`, but this can be changed) and put all your html, css and js files in it.
* Create a `.gitlab-ci.yml` file in the root of the repo so that your repo looks like this:

```
[4.0K]  .
├── [ 205]  .gitlab-ci.yml
└── [4.0K]  public
    ├── [4.0K]  assets
    │   ├── [4.0K]  css
    │   │   ├── [1.7K]  screen-colm-arch-2.css
    │   │   ├── [   0]  screen-colm-arch.css
    │   │   └── [1.7K]  screen.css
    │   ├── [4.0K]  img
    │   │   ├── [ 23K]  calcifer.jpg
    │   │   ├── [146K]  calcifer.png
    │   │   ├── [ 27K]  fruitnveg64wh37.png
    │   │   └── [1.5K]  Green-16x18-spritesheet.png
    │   └── [4.0K]  js
    │       ├── [1.9K]  in-class-initial.js
    │       ├── [1.9K]  initial.js
    │       ├── [ 19K]  nipplejs.js
    │       └── [7.6K]  script.js
    ├── [ 680]  gameplay.html
    └── [1.1K]  index.html
```

Use the following config in your `.gitlab-ci.yml` for this setup

```
pages:
  stage: deploy
  environment: production
  script
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
    expire_in: 1 day
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  ```

### 2. Adjust these setting in your Gitlab Repo

- After having created your repo in Gitlab, go to `Settings` > `CI/CD` and `Expand` the section titled `Runners`

![](Screenshot1gl.png)

- In that section, disable the shared runners and enable the runner listed under `Other available runners`

![](Screenshot2gl.png)

- Your settings page should list the runner as so:

![](Screenshot3gl.png)

### 3. Commit your changes and watch the runner

* Add all these files to your commit and push them to the repo.
* Check the configuration of your repository and look for the General > Pages section. If the `.yml` file is configured correctly you will see a link to your gitlab page, it will look like this https://ui-programming-22-23.gitlab.io/module-chapters
* You can also check the state of the runner, but this is optional.
* Find the public URL of your project by selecting `Deployment` > `Pages` in your repo left sidebar.

## Practical 11:

Do all of this with your repository from practical 10, make it a public gitlab pages website and submit that link to the CA spreadsheet.


