# Practical 1

## Part one: starter web project

Create a web structure including at least:

- 1 html file
- linked CSS file
- linked js file

Your HTML file will make use of five HTML5 tags. Research online what new tags were added to HTML when version 5 came out and use them appropriately in your page. For example, you could use the `<nav>` tag to make a small menu, the `<aside>` tag for a side bar, etc.

Style each of the five HTML5 tags with CSS giving them unique visual attributes. Try to create web page that looks good, avoid clashing colours and jarring design choices.

## Part two: publish your starter web project on gitlab

Create a Gitlab repository for the basic web code you wrote for Part 1.

You will need a Gitlab.com account for which you will use your student email. Make sure you have notified Colm when your account is created, as you need to be added to the `Ui Programming` group for easier project sharing.

### Using `gitbash`  and `gitlab.com`

`gitbash` is a tool that allows all operating systems to have similar interactions with the git tool. It is installed on all the machines on campus, you will have to install it on your personal computer if you are using it for this module. You can launch `gitbash` like any other program or application from the start menu, or you can use your file explorer to navigate to the location on your system where you wish to work, right click and use the `Git Bash Here` option to open the shell in that exact location.

**A small list of commands for file system navigation using gitbash:**

`pwd` → print working directory

`ls` → list files and folders in current directory

`cd 'folderName'` → change directory to specific folder (user by adding the folder name as command line argument: `cd Documents/`)

`cd ..` → move working directory one level up (one step backwards in the file tree)

`cp 'originalFileName' 'newFilename'` → creates a copy of originalFile to newFile

`rm 'fileName'` → deletes the specified file (be careful with `rm` it does not move files to the 'Recycle Bin', deletions are ultimate)

`mkdir 'newFolderName'` → makes a new directory (folder) with the specified newFolderName

### Creating a folder for a practical submission (or another project)

- Start by creating a new project on gitlab.com. Sign in to your account using a web browser and click the button saying 'New Project'. Usually 'Create blank project' is the required next step. Give the project a title. For UI-Programming practicals, you will be required to reuse the practical title, for example `3-web-forms_data-reception-practical-firstName-lastName`.
- !IMPORTANT! in the Project URL field, make sure to select the `ui-programming-21-22` group for the project to appear for the group. You can keep the 'Visibility level' private or public, up to you.
- Intialising the project with a `README.md` can be a good convention but when you are starting off you can un-tick that option, as that will display a helpful guide for the next steps. The guide looks like the following image:

  ![](gitlab-init-help.png)
- Select the correct option from the choices proposed by gitlab above.
- either clone the newly created repo
- or add a remote to an existing folder

### Usual interaction with a git repository

Once your git work folder / project / practical / amazing new website, app or game is intialised, git will be tracking changes as they happen in that specific folder.

Get used to using the `git status` command. (Make sure your terminal / bash session is in the correct location on your file system -- running the 'git status' command in the Documents folder will probably return an error)

`git status` will inform you of what files are currently being tracked by git, what files have changed, what has been deleted and even what has been moved or renamed.

From the onset, you need to tell git what files you want it to track. In a project folder, you might want git to track your code files as they evolve, but other files might not be suited for git. For example, if you are creating a website that will host a video, don't add that video file to git. There are a few reasons for that: firstly, the video file is lightly to be a large file, git is designed to track changes in text and code files not compiled video files. Secondly, the video file is not likely to change in the same way your code will. This video is an external asset; we probably don't need git to keep an eye on it.

`git add index.html` → tells git to start tracking the 'index.html' file
`git add assets/` → tells git to start tracking the 'assets' folder and all its content

Run `git status` to see what git is doing at any time.

Git is a powerful versioning tool that lets you move backwards and forwards in a project history. To enable this, you must get familiar with the practice of commits. Commits or committing is equivalent to creating a 'snapshot' of your project at this current time. Your project will accumulate lots of these different snapshots and these will be the points you can move back to in case you need to revert your code to a previous version.

Once you have added a few files to git with `git add 'file or folder'`  run the following command:
`git commit -m "write a message between these quotemaks that describe the latest changes e.g. added a JS function that toggles dark mode on and off"`

Run `git status` again

Your terminal will say something like "Your branch is ahead of 'origin/main' by 1 commit". This specifically means that your **local** project/repository (the files on the computer you are working on) is now ahead of the repository that is on gitlab.com. To synchronise them, you have one last thing left to do:

`git push`

The command above will take your latest commits and synch them with the versions on gitlab.com. This also creates a backup of your work (the repo is stored on gitlab.com, you can always download the latest code from there).

### A video recap of all of the above

Start at 1:01 for CLI recap.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5HLst694D_Y?start=61" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](https://gitlab.com/relearn/Relearn2013/-/raw/master/cheat-sheet/git-scheme.svg)

